FROM node:lts-alpine3.12
LABEL description="Teste Muntant DevOps" maintainer="Marcelo Henrique Araujo do Bomfim <mhabomfim@gmail.com>"
RUN mkdir -p /home/node/app
WORKDIR /home/node/app
COPY ./app /home/node/app/
RUN npm install
ENV CANDIDATO=""
EXPOSE 3000
CMD [ "node", "index.js" ]
